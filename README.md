ObamaDJ
========


Usage:

- base64-encode your track's URL (replace `+/` with `-_`)
- add `http://obama.ale.sh/ytdl/{b64_url}/{whatever}.m3u8` to your playlist
- party!

Almost anything that [youtube-dl supports] can be played. (Tested on YouTube / Soundcloud)

[youtube-dl supports]: http://rg3.github.io/youtube-dl/supportedsites.html

Example playlist:

```
#EXTM3U
#EXTINF:,[soundcloud] Like That [Nghtmre & Big Gigantic Release]
http://obama.ale.sh/ytdl/aHR0cHM6Ly9zb3VuZGNsb3VkLmNvbS9tcnN0cmltLWpqL2xpa2UtdGhhdC1uZ2h0bXJlLWJpZy1naWdhbnRpYy1yZWxlYXNl/likethat.m3u8
#EXTINF:,[youtube] REOL — Whatever idklol
http://obama.ale.sh/ytdl/aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g_dj1uNUN3WHV5TmZvYw==/this-part-doesnt-matter-at-all.m3u8
```


License
--------
yet to be published
